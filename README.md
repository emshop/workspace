# 产研任务管理系统
* 前端 vue3+elementplus
* 后端 golang+mysql

基于自研的hycli低代码工具生成, 可私有化部署，可定制化开发

在线体验地址:[http://ws.xhydra.cn/ ](http://ws.xhydra.cn/)
admin/123456

### 系统截图
![image](/docs/imgs/001.png)
![image](/docs/imgs/002.png)
![image](/docs/imgs/003.png)
![image](/docs/imgs/004-1.png)
![image](/docs/imgs/004-2.png)
![image](/docs/imgs/004-3.png)
![image](/docs/imgs/004-5.png)
![image](/docs/imgs/004-6.png)
![image](/docs/imgs/004-7.png)
![image](/docs/imgs/004-8.png)
![image](/docs/imgs/005.png)
![image](/docs/imgs/006.png)
![image](/docs/imgs/007.png)
![image](/docs/imgs/008.png)

![image](/docs/imgs/009.png)
![image](/docs/imgs/010.png)
![image](/docs/imgs/011.png)
![image](/docs/imgs/012.png)
![image](/docs/imgs/013.png)
![image](/docs/imgs/014.png)
![image](/docs/imgs/015.png)
![image](/docs/imgs/016.png)
![image](/docs/imgs/017.png)
![image](/docs/imgs/018.png)
![image](/docs/imgs/020.png)
![image](/docs/imgs/090.png)
![image](/docs/imgs/091.png)
![image](/docs/imgs/092.png)
![image](/docs/imgs/093.png)
![image](/docs/imgs/094.png)
![image](/docs/imgs/098.png)
![image](/docs/imgs/099.png)

### 🐿️联系作者
![image](/docs/imgs/wx-p.png)

### 💲捐赠 🥣
![image](/docs/imgs/jz.png)