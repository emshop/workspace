

const routes = [
    {
      path: "/",
      component: () => import("../views/menu/index.vue"),
      children: [
        {
          path: 'ws/dev/plan/:archive_status?',
          component: () => import("../views/ws/dev/dev_plan.list.vue"),
        },
        {
          path: 'ws/project/design/:archive_status?',
          component: () => import("../views/ws/project/project_design.list.vue"),
        },
        {
          path: 'ws/dev/task/:archive_status?/:dev_tp?',
          component: () => import("../views/ws/dev/dev_task.list.vue"),
        },
        {
          path: 'ws/ops/task/:archive_status?',
          component: () => import("../views/ws/ops/ops_task.list.vue"),
        },
        {
          path: 'ws/dev/docs',
          component: () => import("../views/ws/dev/dev_docs.list.vue"),
        },
        {
          path: 'ws/dev/deploy',
          component: () => import("../views/ws/dev/dev_deploy.list.vue"),
        },
        {
          path: 'ws/collab/task/:archive_status?',
          component: () => import("../views/ws/collab/collab_task.list.vue"),
        },
        {
          path: 'ws/dev/risk/:archive_status?',
          component: () => import("../views/ws/dev/dev_risk.list.vue"),
        },
        {
          path: 'ws/prod/requirement/:prstatus?/:archive_status?/:tp?',
          component: () => import("../views/ws/prod/prod_requirement.list.vue"),
        },
        {
          path: 'ws/sys/management/:status?',
          component: () => import("../views/ws/sys/sys_management.list.vue"),
        },
        {
          path: 'ws/prod/tracking',
          component: () => import("../views/ws/prod/prod_tracking.list.vue"),
        },
        {
          path: 'ws/staff/tracking',
          component: () => import("../views/ws/staff/staff_tracking.list.vue"),
        },
        {
          path: 'ws/prod/problem',
          component: () => import("../views/ws/prod/prod_problem.list.vue"),
        },
        {
          path: 'ws/prod/design/:archive_status?',
          component: () => import("../views/ws/prod/prod_design.list.vue"),
        },
        {
          path: 'ws/daily/task/:archive_status?',
          component: () => import("../views/ws/daily/daily_task.list.vue"),
        },
        {
          path: 'ws/corp/culture',
          component: () => import("../views/ws/corp/corp_culture.list.vue"),
        },
        {
          path: 'sso/system/info',
          component: () => import("../views/sso/system/system_info.list.vue"),
        },
        {
          path: 'sso/system/menu',
          component: () => import("../views/sso/system/system_menu.list.vue"),
        },
        {
          path: 'sso/user/account',
          component: () => import("../views/sso/user/user_account.list.vue"),
        },
        {
          path: 'sso/role/info',
          component: () => import("../views/sso/role/role_info.list.vue"),
        },
        {
          path: 'sso/role/menu',
          component: () => import("../views/sso/role/role_menu.list.vue"),
        },
        {
          path: 'sso/user/role',
          component: () => import("../views/sso/user/user_role.list.vue"),
        },
        {
          path: 'sso/user/log',
          component: () => import("../views/sso/user/user_log.list.vue"),
        },
        {
          path: 'ws/test/task/:archive_status?',
          component: () => import("../views/ws/test/test_task.list.vue"),
        },
        {
          path: 'ws/test/bug/:archive_status?',
          component: () => import("../views/ws/test/test_bug.list.vue"),
        },
        {
          path: 'ws/test/bug/history',
          component: () => import("../views/ws/test/test_bug_history.list.vue"),
        },
        {
          path: 'ws/staff/info/:status?',
          component: () => import("../views/ws/staff/staff_info.list.vue"),
        },
        {
          path: 'ws/message/notification',
          component: () => import("../views/ws/message/message_notification.list.vue"),
        },
        {
          path: 'ws/department/info',
          component: () => import("../views/ws/department/department_info.list.vue"),
        },
        {
          path: 'ws/my/note',
          component: () => import("../views/ws/my/my_note.list.vue"),
        },
        {
          path: 'dds/dictionary/info',
          component: () => import("../views/dds/dictionary/dictionary_info.list.vue"),
        },
        {
          path: 'ws/weekly/report',
          component: () => import("../views/ws/weekly/weekly_report.list.vue"),
        },
        {
          path: 'ws/olb/system',
          component: () => import("../views/ws/olb/olb_system.list.vue"),
        },
        {
          path: 'ws/plan/report',
          component: () => import("../views/ws/plan/plan_report.list.vue"),
        },
      ]
    },
  ]
export default routes